//
//  ResponsibilityDemoController.swift
//  DesginPattern
//
//  Created by Guo Longxiang, (Longxiang.Guo@partner.bmw.com) on 2019/9/5.
//  Copyright © 2019 Guo Longxiang, (Longxiang.Guo@partner.bmw.com). All rights reserved.
//

import Foundation
import UIKit

class ResponsibilityDemoController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let groupLeader = GroupLeader()
        let manager = Manager()
        groupLeader.setNext(responsder: manager)
        print("\n=======================================\n")
        
        //reqeust two days leave
        let request1 = LeaveRequest(leaveDays: 2, reason: "", name: "zhangshan")
        groupLeader.handler(reuqest: request1)
        print("\n=======================================\n")
        
        //reqeust four days leave
        let request2 = LeaveRequest(leaveDays: 4, reason: "", name: "lishi")
        groupLeader.handler(reuqest: request2)
        print("\n=======================================\n")
        
    }
}
