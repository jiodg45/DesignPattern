//
//  LeaveRequest.swift
//  DesginPattern
//
//  Created by Guo Longxiang, (Longxiang.Guo@partner.bmw.com) on 2019/9/5.
//  Copyright © 2019 Guo Longxiang, (Longxiang.Guo@partner.bmw.com). All rights reserved.
//

import Foundation

struct LeaveRequest: Codable, CustomStringConvertible {
    var leaveDays: Int
    var reason: String
    var name: String
    
    var description: String {
        return "\(name) reuqest leave \(leaveDays) days,reason: \(reason)"
    }
}


