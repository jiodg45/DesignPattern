//
//  AttendanceResponder.swift
//  DesginPattern
//
//  Created by Guo Longxiang, (Longxiang.Guo@partner.bmw.com) on 2019/9/5.
//  Copyright © 2019 Guo Longxiang, (Longxiang.Guo@partner.bmw.com). All rights reserved.
//

import Foundation

protocol AttendanceResponder {
    
    var nextResponder: AttendanceResponder?  { get }
    
    func setNext(responsder: AttendanceResponder)
    
    func handler(reuqest: LeaveRequest)  
}
