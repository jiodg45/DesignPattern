//
//  GroupLeader.swift
//  DesginPattern
//
//  Created by Guo Longxiang, (Longxiang.Guo@partner.bmw.com) on 2019/9/5.
//  Copyright © 2019 Guo Longxiang, (Longxiang.Guo@partner.bmw.com). All rights reserved.
//

import Foundation

class GroupLeader: AttendanceResponder {
    
    public var nextResponder: AttendanceResponder? {
        return _responder
    }
    
    public func setNext(responsder: AttendanceResponder) {
        _responder = responsder
    }
    
    public func handler(reuqest: LeaveRequest) {
        if reuqest.leaveDays > 3 {
            print("\(self) by pass leave request to next responder")
            nextResponder?.handler(reuqest: reuqest)
        } else {
            print("\(self) accept \(reuqest)")
        }
    }
    
    private var _responder: AttendanceResponder?
    
}
