//
//  Manager.swift
//  DesginPattern
//
//  Created by Guo Longxiang, (Longxiang.Guo@partner.bmw.com) on 2019/9/5.
//  Copyright © 2019 Guo Longxiang, (Longxiang.Guo@partner.bmw.com). All rights reserved.
//

import Foundation

class Manager: AttendanceResponder {
    
    public var nextResponder: AttendanceResponder? {
        return _responder
    }
    
    public func setNext(responsder: AttendanceResponder) {
        _responder = responsder
    }
    
    public func handler(reuqest: LeaveRequest) {
        if reuqest.leaveDays > 3 {
            print("\(self) accept \(reuqest)")
            nextResponder?.handler(reuqest: reuqest)
        } else {
            nextResponder?.handler(reuqest: reuqest)
        }
    }
    
    private var _responder: AttendanceResponder?
    
}

