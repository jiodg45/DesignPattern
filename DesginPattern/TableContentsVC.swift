//
//  TableContentsVC.swift
//  DesginPattern
//
//  Created by Guo Longxiang, (Longxiang.Guo@partner.bmw.com) on 2019/9/5.
//  Copyright © 2019 Guo Longxiang, (Longxiang.Guo@partner.bmw.com). All rights reserved.
//

import UIKit

enum DesginPattern: Int, CaseIterable {
    case construct
    case structure
    case action
}

enum ConstructDesign: String, CaseIterable {
    case factory
    case abscratfactory
    case singleton
    case buid
    case prototype
}

enum StructureDesign: String, CaseIterable {
    case bridge
    case decorate
    case adapter
    case delegate
    case appearance
    case share
    case combination
}

enum ActionDesign: String, CaseIterable {
    case strategy
    case template
    case observer
    case iteration
    case responsibility
    case command
    case memorandum
    case status
    case vist
    case mediator
    case explain
}


class TableContentsVC: UITableViewController {

    private struct Const {
        static let cellIdentifier = "CELL_IDENTIFIER"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: Const.cellIdentifier)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DesginPattern.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case DesginPattern.construct.rawValue:
            return ConstructDesign.allCases.count
        case DesginPattern.structure.rawValue:
            return StructureDesign.allCases.count
        case DesginPattern.action.rawValue:
            return ActionDesign.allCases.count
        default:
            fatalError("\(#function) missing desgin parttern)")
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if let cacheCell = tableView.dequeueReusableCell(withIdentifier: Const.cellIdentifier) {
            cell = cacheCell
        } else {
            cell = UITableViewCell(style: .default, reuseIdentifier: Const.cellIdentifier)
        }
        switch indexPath.section {
        case DesginPattern.construct.rawValue:
            cell.textLabel?.text = ConstructDesign.allCases[indexPath.row].rawValue
            cell.backgroundColor = UIColor(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: 0.9)
        case DesginPattern.structure.rawValue:
            cell.textLabel?.text = StructureDesign.allCases[indexPath.row].rawValue
            cell.backgroundColor = UIColor(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: 0.6)
        case DesginPattern.action.rawValue:
            cell.textLabel?.text = ActionDesign.allCases[indexPath.row].rawValue
            cell.backgroundColor = UIColor(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: 0.3)
        default:
            fatalError("\(#function) missing desgin parttern)")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var designParttern: String
        switch indexPath.section {
        case DesginPattern.construct.rawValue:
            designParttern = ConstructDesign.allCases[indexPath.row].rawValue
        case DesginPattern.structure.rawValue:
            designParttern = StructureDesign.allCases[indexPath.row].rawValue
        case DesginPattern.action.rawValue:
            designParttern = ActionDesign.allCases[indexPath.row].rawValue
        default:
            fatalError("\(#function) missing desgin parttern)")
        }
        let targetName = Bundle.main.infoDictionary?["CFBundleExecutable"] as! String
        let className = "\(targetName).\(designParttern.uppercased(prefix: 1))DemoController"
        guard let desinParttenVC = NSClassFromString(className) as? UIViewController.Type else {
            return
        }
        let vc = desinParttenVC.init()
        vc.title = designParttern
        vc.view.backgroundColor = UIColor.white
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

extension String {
    func uppercased(prefix: Int) -> String {
        guard self.count > prefix, !self.isEmpty else {
            return ""
        }
        let str = self as NSString
        let subStr = self.prefix(prefix).uppercased()
        return  str.replacingCharacters(in: NSMakeRange(0, prefix), with: subStr)
    }
}
